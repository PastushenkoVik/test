<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=utf-8>
	<meta http-equiv= "X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>test Pastushenko</title>	
 	<meta name="description" content="">
	<meta name="keywords" content="">
	
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">
	
	<link rel="shortcut icon" href="">
		
	<link rel="stylesheet" href= "/css/bootstrap.min.css">

	<link rel="stylesheet" href= "/css/animate.min.css">
	<link rel="stylesheet" href= "/css/test.css">

</head>

<body>
	<header>
		<div id="header-menu" class="container-fluid">
			<div class="container">				
				<div class="row">
					<div class="col">
						<a class="navbar-brand" href="http://test.the-brand.in.ua/">
							<img src="img/logo.png" alt="">
						</a>
					</div>
					<nav class="navbar navbar-expand-lg navbar-dark ml-auto">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
					  
						<div id="navbarNav" class="collapse navbar-collapse">
							<ul class="navbar-nav">
								<li id="nav-features" class="nav-item active">
									<span class="nav-link"><?php echo _( 'Features' ); ?></span>
								</li>
								<li id="nav-about" class="nav-item">
									<span class="nav-link"><?php echo _( 'About' ); ?></span>
								</li>
								<li id="nav-pricing" class="nav-item">
									<span class="nav-link"><?php echo _( 'Pricing' ); ?></span>
								</li>
								<li id="nav-reviews" class="nav-item">
									<span class="nav-link"><?php echo _( 'Reviews' ); ?></span>
								</li>
								<li id="nav-contact" class="nav-item">
									<span class="nav-link"><?php echo _( 'Contact' ); ?></span>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
