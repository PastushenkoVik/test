<?php
	require( 'header.php' );
?>
	
<div id="sec-1" class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-5">
				<div class="logo-txt">
					<span>AX</span><span>IT</span>
				</div>
				<p class="big-white white-divider">
					<?php echo sprintf( _( 'MODERN AXURE TEMPLATE%s FOR BEAUTIFUL PROTOTYPES' ), '<br>' ); ?>
				</p>
				<p class="transp-white">
					<?php echo _( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vulputate pellentesque urna eget dictum. Donec consectetur diam eget urna euismod, vel ultrices sem pellentesque.' ); ?>
				</p>
				<button type="button" class="btn trasp-border">
					<?php echo _( 'Download' ); ?>
				</button>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 ml-auto">
				<div class="form-pass">
					<p class="form-pass-title p-bold">
						<?php echo sprintf( _( 'Try Your %sFREE%s Trial Today.' ), '<span class="p-orage">', '</span>' ); ?>
					</p>
					<form>
						<input type="text" class="form-control" placeholder="<?php echo _( 'Name' ); ?>">
						<input type="email" class="form-control" aria-describedby="emailHelp" placeholder="<?php echo _( 'Email' ); ?>">
						<input type="password" class="form-control" placeholder="<?php echo _( 'Password' ); ?>">">
					</form>
					<p class="b-orage p-bold">
						<?php echo _( 'Get Started' ); ?>
					</p>
				</div>
			</div>
		</div>
	</div>	
	<div class="container-fluid so-media">
		<div class="container">
			<div class="row ">
				<p class="p-bold">
					<div class="col-xs-12 col-sm-12 col-md-3 mr-auto">
						<p>
							<?php echo _( 'Social Media' ); ?>
						</p>
						<p>
							<?php echo _( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vulputate pellentesque urna eget dictum.' ); ?>
						</p>
					</div>
					<div class="col-auto">
						<i class="fa fa-facebook" aria-hidden="true"></i>
					</div>
					<div class="col-auto">
						<i class="fa fa-twitter" aria-hidden="true"></i>
					</div>
					<div class="col-auto">
						<i class="fa fa-google-plus" aria-hidden="true"></i>
					</div>
					<div class="col-auto">
						<i class="fa fa-pinterest" aria-hidden="true"></i>
					</div>
					<div class="col-auto">
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</div>
					<div class="col-auto">
						<i class="fa fa-stumbleupon" aria-hidden="true"></i>
					</div>
					<div class="col-auto">
						<i class="fa fa-rss" aria-hidden="true"></i>
					</div>
				</p>			
			</div>
		</div>
	</div>
</div>
<?php
	require( 'footer.php' );
?>
